SHELL:=/bin/bash

P_PASS="nube.coop/ftp/osiux.com/osirisosiux"
P_DIR="$$HOME/.password-store"
R_HOST="ftp.nube.coop"
R_USER="osirisosiux"
R_DIR="web"

PASSWORD=$$(PASSWORD_STORE_DIR=$(P_DIR) pass $(P_PASS))

UP:
	lftp -u $(R_USER),$(PASSWORD) $(R_HOST) -e 'set ssl:verify-certificate no; mirror -R --only-newer --exclude .git --exclude composer.phar --exclude Makefile . web/; quit'

DOWN:
	lftp -u $(R_USER),$(PASSWORD) $(R_HOST) -e 'set ssl:verify-certificate no; mirror --only-newer --exclude .git web/ .;quit'

HTML:
	lftp -u $(R_USER),$(PASSWORD) $(R_HOST) -e 'set ssl:verify-certificate no; mirror -R -i "\.hmtl$$" --only-newer --exclude .git --exclude composer.phar . web/; quit'

INDEX:
	lftp -u $(R_USER),$(PASSWORD) $(R_HOST) -e 'set ssl:verify-certificate no; put -O web/ index.html; quit'

IMAGES:
	lftp -u $(R_USER),$(PASSWORD) $(R_HOST) -e 'set ssl:verify-certificate no;mirror -R --only-newer tmb/ web/tmb/;quit'
	lftp -u $(R_USER),$(PASSWORD) $(R_HOST) -e 'set ssl:verify-certificate no;mirror -R --only-newer img/ web/img/;quit'

MIN:
	java -jar ~/bin/yuicompressor.jar -o css/style.min.css css/style.css

ALL: HTML CSS IMAGES HTACCESS
